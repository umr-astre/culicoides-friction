---
output:
  html_document: default
  pdf_document: default
bibliography: famuvie-culicoides-friction-project.bib
---

# map of the archive (Makefile format)

last_version/landscape\ SJ/script/rugosity_v3.R:
  covars_mer/sea.grd, covars_terre/land.grd

covars_mer/sea.grd, covars_terre/land.grd:
  1_clean_era1/2.R
  






# Culicoides-friction

__Dispersion of _Culicoides imicola_ using friction maps__


## Context

2020-02-24 - Stéphanie Jacquet

The variable difftime from genetenv_cs_WMB_mean_max_final.csv is log(1
+ x) where x is the variable difftime from difftime.csv

We think that `nbdist` is a geographical distance, while `difftime` is
the difference in time (in days, most likely, and then scaled with log).
At some point, the ranking of time differences is computed and stored

2018-07-10 - Ahmadou Dicko

Revised some code. Ahmadou needs to check between different versions of the 
same file to figure out which is the latest.

Built a diagram of file relationships in `doc/diagram_archive.graphml`.

The process tries to converge iteratively to a genetic-cost surface. First, it
assumes a flat cost-surface, where distances between spots are Euclidean. At
each step, it fits a regression of genetic-distance versus a number of
environmental variables which depend on the route connecting the populations
together with a Matérn spatial effect. Using the fitted model, the genetic-cost
is predicted at each pixel in the region, and minimum-cost paths are computed
for all pairs of populations. For each of these pairs, the set of covariables is
updated. The procedure continues until convergence.

The difficulty resides in that a different set of variables is used over land
and over sea, since different mechanisms take place (active vs passive movement,
etc.). So each route needs to be "decomposed" in segments and the genetic
distance needs to be explained in terms of a different set of variables for each
component.



2017-11-20 - Karine Huber

The objective is to understand the dispersal patterns and driving forces of the
species. In particular, the influence of landscape and environmental features.

Through a population-genetics approach, they search to explain the genetic
distance between samples with a cost-based geographical distance.


2017-11-20 - Renaud Lancelot, Thierry Baldet, Karine Huber

- Samples are dispersed around the Mediterranean. One important question is to
understand the dispersal pattern over the sea.


2017-11-23 - Karine Huber, Jérémy Bouyer, Ahmadou Dicko

- Recovered data and scripts from BIOS-J466 (login: Bios-J466, passw: azerty).
Specifically, content from `~/Dropbox` and `~/Publi` copied to Karine's disk.

- Ahmadou identified directory `~/Dropbox/Public/culiwind` as a candidate latest
version and its contents were shared via FileSender.

- I can contact Ahmadou Dicko (dicko.ahmadou@gmail.com) between 8 and 9h for
gidance through the scripts.



2018-05-04 - Karine Huber, Jérémy Bouyer

- Initiated forensic reconstruction of relationships among files
(doc/diagram_archive.graphml)

- Jérémy will try to bring Amadou Dicko to have him clean up the mess.

- Otherwise, continue the reconstruction, and try to reproduce the results.

- Use a Makefile to document dependencies.


## Data


- Genotypes at nine microsatellite loci of $n = 711$ individuals from $31$
sites. (Not from the same year)

- Sequence of COI and CytB genes for $228$ individuals.



## Proposed Analyses

- Build a landscape resistance map to Culicoides genetic-flow from the genetic
distances as in [@Bouyer15Mapping]


## Remarks

- Look at the current state of the code and estimate analysis time.

- Stéphanie (preliminar code). Ahmadou (last version).


## Bibliography